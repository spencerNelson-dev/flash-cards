using System.ComponentModel.DataAnnotations;

namespace flash_cards.models
{
    public class Deck
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string DeckName { get; set; }

        [Required]
        public string Description { get; set; }

        public int OwnerId { get; set; }
    }
}

/*
Id
Name
Description
OwnerId
*/

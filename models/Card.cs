using System.ComponentModel.DataAnnotations;

namespace flash_cards.models
{
    public class Card
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Question { get; set; }

        [Required]
        public string Answer { get; set; }

        [Required]
        public string DeckName { get; set; }
        
    }
}
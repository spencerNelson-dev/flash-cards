using AutoMapper;
using flash_cards.Dtos;
using flash_cards.models;

namespace flash_cards.Profiles
{
    public class DecksProfile : Profile
    {
        public DecksProfile()
        {
            CreateMap<Deck, DeckReadDto>();

            CreateMap<DeckCreateDto, Deck>();

            CreateMap<DeckUpdateDto, Deck>();

            CreateMap<Deck, DeckUpdateDto>();
        }
    }
}
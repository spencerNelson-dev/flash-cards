using AutoMapper;
using flash_cards.Dtos;
using flash_cards.models;

namespace flash_cards.Profiles
{
    public class CardsProfile : Profile
    {
        public CardsProfile()
        {
            // Source --> Target
            CreateMap<Card, CardReadDto>();

            CreateMap<CardCreateDto, Card>();

            CreateMap<CardUpdateDto, Card>();

            CreateMap<Card, CardUpdateDto>();
        }
    }
}
# Define base image
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

# Fetch and install Node 10. Make sure to include the --yes parameter 
# to automatically accept prompts during install, or it'll fail.
RUN curl --silent --location https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install --yes nodejs

# Copy project files
WORKDIR /source
COPY . .

# Restore
RUN dotnet restore "flash-cards.csproj"
COPY . .

# Publish
RUN dotnet publish -c Release -o /publish


# Runtime
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /publish
COPY --from=build-env /publish .
# ENTRYPOINT ["dotnet", "flash-cards.dll"]
# for heroku
CMD ASPNETCORE_URLS=http://*:$PORT ELEPHANTSQL_URL=$ELEPHANTSQL_URL dotnet flash-cards.dll


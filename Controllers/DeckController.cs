using System;
using System.Collections.Generic;
using AutoMapper;
using flash_cards.Data;
using flash_cards.Dtos;
using flash_cards.models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace flash_cards.Controllers
{
    [ApiController]
    [Route("api/decks")]
    public class DeckController : ControllerBase
    {
        private readonly ILogger<DeckController> _logger;
        private readonly IDeckRepo _repository;
        private readonly IMapper _mapper;

        public DeckController(ILogger<DeckController> logger, IDeckRepo repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        // GET api/decks
        [HttpGet]
        public ActionResult<IEnumerable<DeckReadDto>> GetAllDecks()
        {
            var decks = _repository.GetAllDecks();

            return Ok(_mapper.Map<IEnumerable<DeckReadDto>>(decks));
        }

        // GET api/decks/{id}
        [HttpGet("{id}", Name = "GetDeckById")]
        public ActionResult<DeckReadDto> GetDeckById(int Id)
        {
            var deckItem = _repository.GetDeckById(Id);

            if (deckItem != null)
            {
                return Ok(_mapper.Map<DeckReadDto>(deckItem));
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/decks
        [HttpPost]
        public ActionResult<DeckReadDto> CreateDeck(DeckCreateDto deckCreateDto)
        {
            var deckModel = _mapper.Map<Deck>(deckCreateDto);

            _repository.CreateDeck(deckModel);
            _repository.SaveChanges();

            var deckReadDto = _mapper.Map<DeckReadDto>(deckModel);

            return CreatedAtRoute(nameof(GetDeckById), new { id = deckReadDto.Id }, deckReadDto);
        }

        // PUT api/decks/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateDeck(int id, DeckUpdateDto deckUpdateDto)
        {
            var deckModelFromRepo = _repository.GetDeckById(id);
            if (deckModelFromRepo == null)
            {
                return NotFound();
            }

            _mapper.Map(deckUpdateDto, deckModelFromRepo);

            _repository.UpdateDeck(deckModelFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }

        // PATCH api/decks/{id}
        /* 
            A patch request looks like the following:
            [
                {
                    "op": "replace",
                    "path":"/description",
                    "value":"PATCH"
                }
            ]
        */
        [HttpPatch("{id}")]
        public ActionResult PartialDeckUpdate(int id, JsonPatchDocument<DeckUpdateDto> patchDoc)
        {
            var deckModelFromRepo = _repository.GetDeckById(id);
            if (deckModelFromRepo == null)
            {
                return NotFound();
            }

            var deckToPatch = _mapper.Map<DeckUpdateDto>(deckModelFromRepo);

            patchDoc.ApplyTo(deckToPatch, ModelState);

            //validation check
            if (!TryValidateModel(deckToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(deckToPatch, deckModelFromRepo);

            _repository.UpdateDeck(deckModelFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }

        // DELETE api/decks/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteDeck(int id)
        {
            var deckModelFromRepo = _repository.GetDeckById(id);
            if (deckModelFromRepo == null)
            {
                return NotFound();
            }

            _repository.DeleteDeck(deckModelFromRepo);

            _repository.SaveChanges();

            return Ok(deckModelFromRepo);
        }
    }
}

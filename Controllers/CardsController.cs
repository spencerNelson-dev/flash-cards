using System.Collections.Generic;
using AutoMapper;
using flash_cards.Data;
using flash_cards.Dtos;
using flash_cards.models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace flash_cards.Controllers
{
    //api/[controller]
    [Route("api/cards")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private readonly IFlashCardsRepo _respository;
        private readonly IMapper _mapper;

        public CardsController(IFlashCardsRepo repository, IMapper mapper)
        {
            _respository = repository;
            _mapper = mapper;
        }

        // private readonly MockCardRepo _respository = new MockCardRepo();

        // GET api/cards
        [HttpGet]
        public ActionResult<IEnumerable<CardReadDto>> GetAllCards()
        {
            var cardItems = _respository.GetAllCards();

            return Ok(_mapper.Map<IEnumerable<CardReadDto>>(cardItems));
        }

        // GET api/cards/{id}
        [HttpGet("{id}", Name = "GetCardById")]
        public ActionResult<CardReadDto> GetCardById(int Id)
        {
            var cardItem = _respository.GetCardById(Id);

            if (cardItem != null)
            {
                return Ok(_mapper.Map<CardReadDto>(cardItem));
            }
            else
            {
                return NotFound();
            }


        }

        // POST api/cards
        [HttpPost]
        public ActionResult<CardReadDto> CreateCard(CardCreateDto cardCreateDto)
        {
            var cardModel = _mapper.Map<Card>(cardCreateDto);

            _respository.CreateCard(cardModel);
            _respository.SaveChanges();

            var cardReadDto = _mapper.Map<CardReadDto>(cardModel);

            // name of route, object with route values, returned object
            return CreatedAtRoute(nameof(GetCardById), new { Id = cardReadDto.Id }, cardReadDto);
        }

        // PUT api/cards/{id}
        [HttpPut("{Id}")]
        public ActionResult UpdateCard(int id, CardUpdateDto cardUpdateDto)
        {
            var cardModelFromRepo = _respository.GetCardById(id);
            if (cardModelFromRepo == null)
            {
                return NotFound();
            }

            //(mapFrom, mapTo)
            _mapper.Map(cardUpdateDto, cardModelFromRepo);

            // The mapper updates the database because of
            // entity framework, but this is added if 
            // it needs to be different in the future
            // so these two lines could be taken out
            _respository.UpdateCard(cardModelFromRepo);
            _respository.SaveChanges();

            return NoContent();

        }

        // PATCH api/cards/{id}
        [HttpPatch("{Id}")]
        public ActionResult PartialCardUpdate(int id, JsonPatchDocument<CardUpdateDto> patchDoc)
        {
            var cardModelFromRepo = _respository.GetCardById(id);
            if (cardModelFromRepo == null)
            {
                return NotFound();
            }

            var cardToPatch = _mapper.Map<CardUpdateDto>(cardModelFromRepo);

            patchDoc.ApplyTo(cardToPatch, ModelState);

            //validation check
            if (!TryValidateModel(cardToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(cardToPatch, cardModelFromRepo);

            _respository.UpdateCard(cardModelFromRepo);
            _respository.SaveChanges();

            return NoContent();
        }
    
        // DELETE api/cards{id}
        [HttpDelete("{Id}")]
        public ActionResult DeleteCard(int id)
        {
            var cardModelFromRepo = _respository.GetCardById(id);
            if(cardModelFromRepo == null)
            {
                return NotFound();
            }
            _respository.DeleteCard(cardModelFromRepo);

            _respository.SaveChanges();

            return Ok(cardModelFromRepo);
        }
    }
}
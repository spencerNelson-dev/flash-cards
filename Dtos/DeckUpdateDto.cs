using System.ComponentModel.DataAnnotations;

namespace flash_cards.Dtos
{
    public class DeckUpdateDto
    {
        [Required]
        public string DeckName { get; set; }

        [Required]
        public string Description { get; set; }

        public int OwnerId { get; set; }
    }
}
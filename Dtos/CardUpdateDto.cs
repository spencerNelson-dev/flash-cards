using System.ComponentModel.DataAnnotations;

namespace flash_cards.Dtos
{
    public class CardUpdateDto
    {
        [Required]
        public string Question { get; set; }

        [Required]
        public string Answer { get; set; }

        [Required]
        public string DeckName { get; set; }
        
    }
}
// This code is the exact same as the CreateDto
// This doesn't really need to be created but
// is for clarity and/or a future where crate and update
// need to be different
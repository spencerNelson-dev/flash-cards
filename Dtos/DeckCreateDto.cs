using System.ComponentModel.DataAnnotations;

namespace flash_cards.Dtos
{
    public class DeckCreateDto
    {
        [Required]
        public string DeckName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int OwnerId { get; set; }
    }
}
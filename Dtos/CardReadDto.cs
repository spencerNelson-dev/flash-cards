namespace flash_cards.Dtos
{
    public class CardReadDto
    {
        public int Id { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public string DeckName { get; set; }
        
    }
}
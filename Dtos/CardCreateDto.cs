using System.ComponentModel.DataAnnotations;

namespace flash_cards.Dtos
{
    public class CardCreateDto
    {
        [Required]
        public string Question { get; set; }

        [Required]
        public string Answer { get; set; }

        [Required]
        public string DeckName { get; set; }
        
    }
}
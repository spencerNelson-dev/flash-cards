namespace flash_cards.Dtos
{
    public class DeckReadDto
    {
        public int Id { get; set; }

        public string DeckName { get; set; }

        public string Description { get; set; }

        public int OwnerId { get; set; }
    }
}
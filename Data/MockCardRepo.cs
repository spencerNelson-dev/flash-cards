using System.Collections.Generic;
using flash_cards.models;

namespace flash_cards.Data
{
    public class MockCardRepo : IFlashCardsRepo
    {
        public void CreateCard(Card card)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteCard(Card card)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Card> GetAllCards()
        {
            var cards = new List<Card>
            {
                new Card {Id=0, Question="Capital of Wyoming", Answer="Cheyenne", DeckName="State Capitals"},
                new Card {Id=1, Question="Capital of Utah", Answer="Salt Lake City", DeckName="State Capitals"},
                new Card {Id=2, Question="Capital of Colorado", Answer="Denver", DeckName="State Capitals"}
            };

            return cards;
        }

        public Card GetCardById(int Id)
        {
            return new Card {Id=0, Question="Capital of Wyoming", Answer="Cheyenne", DeckName="State Capitals"};
        }

        public bool SaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateCard(Card card)
        {
            throw new System.NotImplementedException();
        }
    }
}
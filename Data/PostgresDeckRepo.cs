using System;
using System.Collections.Generic;
using System.Linq;
using flash_cards.models;

namespace flash_cards.Data
{
    public class PostgresDeckRepo : IDeckRepo
    {
        private readonly FlashCardsContext _context;

        public PostgresDeckRepo(FlashCardsContext context)
        {
            _context = context;
        }

        public void CreateDeck(Deck deck)
        {
            if (deck == null)
            {
                throw new ArgumentNullException(nameof(deck));
            }

            _context.Decks.Add(deck);
        }

        public void DeleteDeck(Deck deck)
        {
            if (deck == null)
            {
                throw new ArgumentNullException();
            }

            _context.Decks.Remove(deck);
        }

        public IEnumerable<Deck> GetAllDecks()
        {
            return _context.Decks.ToList();
        }

        public Deck GetDeckById(int Id)
        {
            return _context.Decks.FirstOrDefault(p => p.Id == Id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateDeck(Deck deck)
        {
            // Nothing
        }
    }
}
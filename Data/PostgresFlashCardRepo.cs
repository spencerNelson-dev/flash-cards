using System;
using System.Collections.Generic;
using System.Linq;
using flash_cards.models;

namespace flash_cards.Data
{
    public class PostgresFlashCardsRepo : IFlashCardsRepo
    {
        private readonly FlashCardsContext _context;

        public PostgresFlashCardsRepo(FlashCardsContext context)
        {
            _context = context;
        }

        public void CreateCard(Card card)
        {
            if(card == null)
            {
                throw new ArgumentNullException(nameof(card));
            }

            _context.Cards.Add(card);
        }

        public void DeleteCard(Card card)
        {
            if(card == null)
            {
                throw new ArgumentNullException();
            }

            _context.Cards.Remove(card);
        }

        public IEnumerable<Card> GetAllCards()
        {
            return _context.Cards.ToList();
        }

        public Card GetCardById(int Id)
        {
            return _context.Cards.FirstOrDefault(p => p.Id == Id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateCard(Card card)
        {
            // Nothing
        }
    }
}
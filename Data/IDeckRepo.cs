using System.Collections.Generic;
using flash_cards.models;

namespace flash_cards.Data
{
    public interface IDeckRepo
    {
        bool SaveChanges();

        IEnumerable<Deck> GetAllDecks();

        Deck GetDeckById(int Id);

        void CreateDeck(Deck deck);

        void UpdateDeck(Deck deck);

        void DeleteDeck(Deck deck);
    }
}
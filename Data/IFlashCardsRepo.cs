using System.Collections.Generic;
using flash_cards.models;

namespace flash_cards.Data
{
    public interface IFlashCardsRepo
    {
        bool SaveChanges();

        IEnumerable<Card> GetAllCards();

        Card GetCardById(int Id);

        void CreateCard(Card card);

        void UpdateCard(Card card);

        void DeleteCard(Card card);
    }
}
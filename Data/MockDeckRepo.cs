using System.Collections.Generic;
using flash_cards.models;

namespace flash_cards.Data
{
    public class MockDeckReop : IDeckRepo
    {
        private List<Deck> decks = new List<Deck>();

        public void CreateDeck(Deck deck)
        {
            decks.Add(deck);
        }

        public void DeleteDeck(Deck deck)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Deck> GetAllDecks()
        {
            return decks;
        }

        public Deck GetDeckById(int Id)
        {
            var output = new Deck();

            foreach(var deck in decks)
            {
                if(deck.Id == Id)
                {
                    output = deck;
                }
            }

            return output;
        }

        public bool SaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateDeck(Deck deck)
        {
            throw new System.NotImplementedException();
        }
    }
}
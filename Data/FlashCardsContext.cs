using flash_cards.models;
using Microsoft.EntityFrameworkCore;

namespace flash_cards.Data
{
    public class FlashCardsContext : DbContext
    {
        public FlashCardsContext(DbContextOptions<FlashCardsContext> opt) : base(opt)
        {
            
        }

        public DbSet<Card> Cards { get; set; }

        public DbSet<Deck> Decks { get; set; }

        //add DbSets for each model
    }
}
import React, { useContext } from 'react';
import { ListGroup, ListGroupItem, UncontrolledCollapse, Button } from 'reactstrap'
import { DecksContext } from '../1_Contexts/DeckContext'

function DecksPage(props) {

    const { decks, cards, populateDeckList, populateCardList } = useContext(DecksContext)

    const style = {
        active: {
            backgroundColor: "rgba(0, 0, 0, .05)",
            borderColor: "gray",
            color: "#212529"
        },
        link_button: {
            margin: 0,
            padding: 0
        }
    }

    const onClickDelete = async (id) => {

        let response = await fetch(`api/decks/${id}`, {
            method: "DELETE"
        })

        if (!response.ok) {
            alert("Delete failed")
        }
        else {
            populateDeckList()
        }

    }

    const onClickAddCard = async (deckName) => {
        console.log(deckName)

        let body = {
            question: "Test question",
            answer: "Test Answer",
            deckName: deckName
        }

        // test code
        let response = await fetch(`api/cards`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        })
        .catch(error => {
            console.log("catch",error)
        })

        if (!response.ok) {
            alert("Post failed")
        }
        else {
            populateCardList()
        }
        
    }

    return (
        <div>
            <div>
                <ListGroup>
                    <ListGroupItem active style={style.active}>
                        <h2>Decks</h2>
                        <p>Here you can view, add, delete, and edit decks.</p>
                    </ListGroupItem>
                    {
                        decks.map((deck, index) => {

                            let id = deck.deckName + deck.id
                            id = id.split(" ").join("");

                            return (
                                <div key={deck.id}>
                                    <ListGroupItem color="success" id={id} tag="button" action>{deck.deckName}</ListGroupItem>
                                    <UncontrolledCollapse toggler={"#" + id}>
                                        <ListGroupItem>
                                            <div>
                                                {deck.description}
                                                <hr />
                                                <Button
                                                    id={"test" + deck.deckName[0]}
                                                    style={style.link_button}
                                                    color="link">
                                                    Show Cards
                                                </Button>
                                                <Button
                                                disabled
                                                onClick={() => {onClickAddCard(deck.deckName)}}
                                                color="link">
                                                    Add Card
                                                </Button>
                                            </div>
                                        </ListGroupItem>
                                        <UncontrolledCollapse toggler={"#test" + deck.deckName[0]}>
                                            <ListGroupItem>
                                                <ListGroup>
                                                    {
                                                        cards.filter((card) =>

                                                            card.deckName === deck.deckName
                                                        )
                                                            .map((card) => {
                                                                return (
                                                                    <ListGroupItem key={card.id}>
                                                                        <div>
                                                                        {`Q: ${card.question} | A: ${card.answer}`}
                                                                        <Button color="link">Edit</Button>
                                                                        <Button color="link">Delete</Button>
                                                                        </div> 
                                                                    </ListGroupItem>
                                                                )
                                                            })
                                                    }
                                                </ListGroup>
                                            </ListGroupItem>
                                        </UncontrolledCollapse>
                                        <ListGroupItem color="danger">
                                            <Button disabled onClick={() => onClickDelete(deck.id)}>Delete</Button>
                                        </ListGroupItem>
                                    </UncontrolledCollapse>
                                </div>
                            )
                        })
                    }
                </ListGroup>
            </div>
        </div>
    );
}

export default DecksPage;
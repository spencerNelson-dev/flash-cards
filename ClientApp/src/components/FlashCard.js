import React, { useState } from 'react';
import {
    Card, CardBody, CardHeader,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';

const style = {
    button: {
        marginTop: 10
    }
}

function FlashCard(props) {

    const [state, setState] = useState(props.card)

    const [toggleFlip, setToggleFlip] = useState(false)

    const onFlipClick = () => {
        setToggleFlip(!toggleFlip)
    }

    return (

        <Card>
            <CardHeader>{state.deckName}</CardHeader>
            <CardBody>
                <CardTitle>
                    {toggleFlip ? "Answer: " : "Question: "}
                </CardTitle>
                <CardSubtitle>
                    {toggleFlip ? state.answer : state.question}
                </CardSubtitle>
                <Button style={style.button} onClick={onFlipClick}>Flip</Button>
            </CardBody>
        </Card>

    );
}

export default FlashCard;
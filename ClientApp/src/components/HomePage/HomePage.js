import React from 'react';
import UserStories from '../zOther/UserStories'

function HomePage(props) {
    return (
        <div>
            This is the landing / home page for our site.
            <div>
                <UserStories></UserStories>
            </div>
        </div>
    );
}

export default HomePage;
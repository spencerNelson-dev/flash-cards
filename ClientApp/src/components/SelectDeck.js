import React, { useState, useContext } from 'react';
import {
    ButtonDropdown, DropdownToggle,
    DropdownMenu, DropdownItem
} from 'reactstrap'
import { DecksContext } from './1_Contexts/DeckContext';


function SelectDeck(props) {

    const [dropdownOpen, setDropdownOpen] = useState(false)
    const [buttonText, setButtonText] = useState("Select Deck")
    const { decks } = useContext(DecksContext)

    const toggleDropDown = () => { setDropdownOpen(!dropdownOpen) }

    const style = {
        display: "inline",
        marginBottom: 5
    }

    const onDeckSelect = (event) => {

        setButtonText(event.target.name)
    }

    return (
        <ButtonDropdown style={style} addonType="prepend" isOpen={dropdownOpen} toggle={toggleDropDown}>
            <DropdownToggle caret>
                {buttonText}
            </DropdownToggle>
            <DropdownMenu>
                {
                    decks.map((value ,index) => {

                        return (
                            <DropdownItem key={index} onClick={onDeckSelect} name={value.deckName}>{value.deckName}</DropdownItem>
                        )
                    })
                }
            </DropdownMenu>
        </ButtonDropdown>
    );
}

export default SelectDeck;
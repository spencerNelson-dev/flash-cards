import React, {useState, useEffect} from 'react';

const DecksContext = React.createContext()

const DecksProvider = (props) => {

    const [decks, setDecks] = useState([])
    const [cards, setCards] = useState([])
    const [selectedDeck, setSelectedDeck] = useState({})

    const populateDeckList = async () => {

        let response = await fetch("api/decks")
        let data = await response.json()

        setDecks(data);
    }

    const populateCardList = async () => {
        let response = await fetch("api/cards")
        let data = await response.json()

        setCards(data)
    }

    useEffect(() => {
        populateDeckList()
        populateCardList()
    }, [])

    return (
        <DecksContext.Provider value={{ decks, setDecks, cards, setCards, selectedDeck, setSelectedDeck, populateDeckList, populateCardList }}>
            {props.children}
        </DecksContext.Provider>
    )
}

const DecksConsumer = DecksContext.Consumer
export { DecksProvider, DecksConsumer, DecksContext }

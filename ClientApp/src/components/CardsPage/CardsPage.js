import React from 'react';
import TestFetch from '../zOther/TestFetch'

function CardsPage(props) {
    return (
        <div>
            Here we can view, add, delete and modify existing cards.
            <TestFetch></TestFetch>
        </div>
    );
}

export default CardsPage;
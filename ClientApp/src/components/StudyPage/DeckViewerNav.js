import React from 'react';
import { Button, ButtonGroup } from 'reactstrap'

const componentStyle = {
    container: {
        textAlign: "center",
        marginTop: "20px"
    }
}

function DeckViewerNav(props) {
    return (
        <div style={componentStyle.container}>
            <ButtonGroup>
                <Button>First</Button>
                <Button onClick={props.clickPrev}>Prev.</Button>
                <Button onClick={props.clickRandom}>Random</Button>
                <Button onClick={props.clickNext}>Next</Button>
                <Button>Last</Button>
            </ButtonGroup>
        </div>
    );
}

export default DeckViewerNav;
import React, { useState } from 'react';
import { CardDeck } from 'reactstrap';

import dummy_deck from '../../dummy_data.json'
import FlashCard from '../FlashCard';
import DeckViewerNav from './DeckViewerNav';

function DeckViewer(props) {

    const [deck, setDeck] = useState(dummy_deck.cards)
    const [index, setIndex] = useState(0)

    const clickPrev = () => {
        let newIndex = index;

        if (index > 0) {
            newIndex--

            setIndex(newIndex)
        }
    }

    const clickNext = () => {
        let newIndex = index;

        if (index < deck.length - 1) {
            newIndex++

            setIndex(newIndex)
        }
    }

    const clickRandom = () => {
        let max = deck.length - 1

        let newIndex = Math.floor(Math.random() * max)

        setIndex(newIndex)
    }

    return (
        <div>

            <div style={{ margin: "0 20% 0 20%" }}>
                
                <div>
                    {`Card ${index + 1} of ${deck.length}`}
                </div>

                <CardDeck>
                    <FlashCard key={index} card={deck[index]} />
                </CardDeck>

                <DeckViewerNav clickNext={clickNext} clickPrev={clickPrev} clickRandom={clickRandom} />

            </div>
        </div>

    )
}

export default DeckViewer;
import React from 'react';
import SelectDeck from '../SelectDeck';
import DeckViewer from './DeckViewer';

function StudyPage(props) {
    return (
        <div>
            <div>
                <h1>Study: <SelectDeck /></h1>
            </div>
            <div>
                <DeckViewer />
            </div>
        </div>
    );
}

export default StudyPage;
import React from 'react';

function UserStories(props) {
    return (
        <div>
        <h1>Flash Card App</h1>
        <p>User Stories:</p>
        <ul>
            <li>User can sign up for an account.</li>
            <li>User can login.</li>
            <li>User can Create, Update and Delete cards.</li>
            <li>User can Create, Update and Delete decks.</li>
            <li>User can flip cards over.</li>
            <li>User can cycle through a deck.</li>
            <li>User can shuffle the deck.</li>
        </ul>
      </div>
    );
}

export default UserStories;
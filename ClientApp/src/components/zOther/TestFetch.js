import React, { useState, useEffect } from 'react';
import {Spinner} from 'reactstrap'

function TestFetch(props) {

    const [cards, setCards] = useState({ cards: [], loading: true })

    const populateData = async () => {
        try
        {
            const response = await fetch('api/cards');
            const data = await response.json();
            setCards({ cards: data, loading: false });
        }
        catch(error)
        {
            console.log(error)
        }
    }

    const renderCardsTable = (cards) => {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Deck Name</th>
                        <th>Question</th>
                        <th>Answer</th>
                    </tr>
                </thead>
                <tbody>
                    {cards.map(deck =>
                        <tr key={deck.id}>
                            <td>{deck.deckName}</td>
                            <td>{deck.question}</td>
                            <td>{deck.answer}</td>

                        </tr>
                    )}
                </tbody>
            </table>
        )
    }

    let contents = cards.loading
        ? <Spinner />
        : renderCardsTable(cards.cards);


    useEffect(() => {
        populateData()
    }, [])

    
    return (
        <div>
            <h1 id="tabelLabel" >Cards</h1>
            <p>This component demonstrates fetching data from the server.</p>
            {contents}
        </div>
    );
}

export default TestFetch;
import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/1_Contexts/Layout';

import './custom.css'

import { DecksProvider } from './components/1_Contexts/DeckContext';
import StudyPage from './components/StudyPage/StudyPage';
import HomePage from './components/HomePage/HomePage';
import CardsPage from './components/CardsPage/CardsPage';
import DecksPage from './components/DecksPage/DecksPage';

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <DecksProvider>
          <Route exact path='/' component={HomePage} />
          <Route path='/study' component={StudyPage} />
          <Route path='/edit-decks' component={DecksPage} />
          <Route path='/edit-cards' component={CardsPage} />
        </DecksProvider>
      </Layout>
    );
  }
}
